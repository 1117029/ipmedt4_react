import React, { Fragment } from "react";
import { useForm } from "react-hook-form";
import { createContext, useContext } from "react";


import { useState } from "react";
import { UserContext } from "../../Context/user";
import Navigation from "../../Navigatie/topAppBar/Navigation";
import "./Home.css";
import axios from "axios";
import { useEffect } from "react";
import useDatafetching from "../Groep/FetchDataGroep";

const Home = () => {

  const user = JSON.parse(localStorage.getItem("userInfo"));

  const { user2 } = useContext(UserContext);
  
  const TEST_URL = "http://127.0.0.1:8000/api/";
  
  const [userRole, SetUserRole] = useState(true);
  const [infoText, setInfoText] = useState([]);



  const [mededelingText, mededelingSetText] = useState({
    Titel: "Er zijn geen mededelingen",
  });

  const { register, handleSubmit } = useForm();

  const onSubmit = data => {
    axios
      .post("http://127.0.0.1:8000/api/maakmededeling", data, {
        headers: { "Content-Type": "application/json" },
      })
      .then(res => {
        console.log(res.data);
        window.location.reload();
      })
      .catch(error => {
        console.log(error.response);
      });
  };

  const [show, toggleShowDivForm] = React.useState(false);
  const showDiv = () => {
    toggleShowDivForm(!show);
  };
  
   const makeFetch = async (baseUrl) => {
    axios
    .get("http://127.0.0.1:8000/api" + baseUrl, {})
    .then(res => {
      if (res.status === 200) {
        console.log(res.data);

        let groepInfo = [];
        groepInfo.push(
          res.data[0].Naam,
          res.data[0].Achternaam,
        );

        if(res.data[0].groep.length > 0){
          groepInfo.push(res.data[0].groep[0].GroepsNaam)
        }else{
          groepInfo.push("Je zit nog niet in een groep")
        }
        
        if(res.data[0].groep[0].subgroep.length > 0){
          groepInfo.push(res.data[0].groep[0].subgroep[0].Subgroepsnaam)
        }else{
          groepInfo.push("Je zit nog niet in een subgroep")
        }
        
        setInfoText(groepInfo);    
        return res.data;

      } else {
      }
    })
    .catch(error => {
      console.log(error.response);
    });
  }

  useEffect(() => {
    //laat de mededelingen zien


    if(user.Role == "lid"){
      SetUserRole(false);
    }

    if(Object.keys(user).length >0){
      makeFetch("/user/"+ user.id +"/")
    }
    
    axios
      .get("http://127.0.0.1:8000/api/laatstemededeling/"+ user.VerenigingNaam, )
      .then(res => {
        if (res.status === 200) {
          console.log(res.data);
          res.data.created_at = res.data.created_at.slice(0, 10);

          mededelingSetText(res.data);

        } else {
        }
      })
      .catch(error => {
        console.log(error.response);
      });
  }, []);


  const deleteMededeling = () => {
    if (window.confirm("weet je zeker dat je dit wilt verwijderen")) {
      axios
        .delete(
          "http://127.0.0.1:8000/api/verwijderMededeling/" + mededelingText.id,
          {}
        )
        .then(res => {
          console.log(res.data);
          window.location.reload();
        })
        .catch(error => {
          console.log(error.response);
        });
    }
  };




  return (
    <Fragment>
      <header>
        <Navigation />
      </header>
      <h1 id="mededeling__titel">Hoofdpagina</h1>

      <section className="info">

          <section className="info__section__groep">
            <h2 className="info__section__h2">Groep: </h2>
            {}
            <h2 className="info__section__h3" >{infoText[2]}</h2>
            {}
          </section>
          <section className="info__section__subgroep">
            <h2 className="info__section__h2">Subgroep:</h2>
            <h2 className="info__section__h3" >{infoText[3]}</h2>
          </section>

          <section id="mededeling__section" className="mededeling__section">
          <h2 id="mededeling__section__h2">Mededeling</h2>
          <h3 id="mededeling__section__h2">{mededelingText.Titel}</h3>
          <p id="mededeling__section__text">{mededelingText.Beschrijving}</p>
          <p id="mededeling__section__date">Gemaakt op:{mededelingText.created_at}</p>
          </section>
          {userRole ? (
            <section className="mededeling__section__button">
                <button class="mededeling__button__make" onClick={showDiv}>Maak een mededeling</button>
                <button class="mededeling__button__delete" onClick={deleteMededeling}> Delete mededeling </button>
            </section>          
           ) : (
             <h3></h3>
           )}

        

      </section>


      {show && (
        <section className="container__section">
          <button className="container__button__x" onClick={showDiv}>X</button>
          <form className="" method="post" onSubmit={handleSubmit(onSubmit)}>
          <input className="container__vereniging" value={user.VerenigingNaam} name="VerenigingNaam" ref={register}></input>
            <p className="container__titel">Titel</p>
            <input
              className="container__input"
              type="text"
              name="Titel"
              ref={register}></input>
            <p className="container__titel">Beschrijving</p>
            <textarea
              className="container__input container__textarea"
              type="text"
              name="Beschrijving"
              ref={register}></textarea>
            <button
              className="container__button__make"
              type="submit"
              value="Verzend het form">Post mededeling</button>
          </form>
        </section>
      )}
    </Fragment>
  );
};
export default Home;