import React, { Fragment } from "react";
import axios from "axios";
import DisplayLedenDelete from "./../Groep/GroepIndelen/DisplayLedenDelete";
import useDatafetching from "../Groep/FetchDataGroep";
import Navigation from "./../../Navigatie/topAppBar/Navigation";
import { useHistory, Link } from "react-router-dom";
import { ReactComponent as TerugIcon } from "../../Icons/arrow_back-24px.svg";

const DeleteSubGroepLeden = ({ match }) => {
  const {
    params: { groepsNaam, subgroepNaam },
  } = match;

  let clickedList = [];

  const TEST_URL = "http://127.0.0.1:8000/api/";
  const history = useHistory();
  const terug = "/groep/" + groepsNaam + "/" + subgroepNaam + "/leden";

  const gebruiker = JSON.parse(localStorage.getItem("userInfo"));

  const { groepId } = useDatafetching(
    TEST_URL + gebruiker.VerenigingNaam + "/" + groepsNaam + "/" + subgroepNaam
  );

  const { user } = useDatafetching(
    TEST_URL +
      gebruiker.VerenigingNaam +
      "/" +
      groepsNaam +
      "/" +
      subgroepNaam +
      "/delete"
  );

  const simpleFunc = value => {
    if (clickedList.some(code => code.user_id === value.user_id)) {
      clickedList = clickedList.filter(code => code.user_id !== value.user_id);
    } else {
      clickedList.push(value);
    }
  };

  const postRequest = clickedList => {
    for (let i = 0; i < clickedList.length; i++) {
      let jsonList = JSON.stringify(clickedList[i]);
      axios
        .put(TEST_URL + "subgroep/users/delete", jsonList, {
          headers: { "Content-Type": "application/json" },
        })
        .then(res => {
          console.log(res);
        })
        .catch(error => {
          console.log(error.response);
        });
    }
  };

  return (
    <Fragment>
      <Navigation />
      <div className="Terug">
        <Link to={terug}>
          <TerugIcon />
          <span>Terug</span>
        </Link>
      </div>
      <section className="veranderGroep">
        <article className="Leden__Article">
          <h1 className="Leden__h1">{subgroepNaam} lid Verwijderen </h1>
          <p>
            Klik op een lid om deze te <b>verwijderen</b> van een groep
          </p>

          <button
            type="submit"
            className="Leden__buttonDelete"
            onClick={() => {
              postRequest(clickedList);
              history.goBack();
            }}>
            Verwijder Leden Uit {subgroepNaam}
          </button>
        </article>
        <div className="Leden__Grid">
          {user.map(user => (
            <div
              onClick={() =>
                simpleFunc({ subgroep_id: groepId, user_id: user.id })
              }>
              <DisplayLedenDelete
                key={user.id}
                naam={user.Naam}
                achternaam={user.Achternaam}
                geboortedatum={user.Datum}></DisplayLedenDelete>
            </div>
          ))}
        </div>
      </section>
    </Fragment>
  );
};

export default DeleteSubGroepLeden;
