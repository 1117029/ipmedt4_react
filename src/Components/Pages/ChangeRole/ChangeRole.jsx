import React, { useContext, useEffect, useState, Fragment} from 'react';
import "./ChangeRole.css";
import axios from 'axios';
import {UserContext} from "../../Context/user"
import Navigation from '../../Navigatie/topAppBar/Navigation';


const ChangeRole = () => {
    const {user} = useContext(UserContext);
    const [users, setUsers] = useState([]);
    let admin = [];
    let leden = [];

    //er worden alleen users zonder de admin role weergegeven.
    const useFetch = (url) => {
        useEffect(()=>{
            setUsers([])
            fetch(url)
            .then(response => response.json())
            .then(getData => setUsers(getData));
        }, [url]);
        return users;
    };
    //leest alleen de users uit van dezelfde vereniging
    useFetch('http://127.0.0.1:8000/api/' + user.VerenigingNaam +'/user');

    const checkRole = (users) => {
        for(let i = 0; i < users.length; i++){
            if(users[i].Role === 'lid'){   
                //add de users met de rol lid in de leden lijst
                const lid = users[i];
                leden.push(lid);
            }
        }
    }
    checkRole(users)

    //voegt de leden die admin mogen toe aan een lijst en houd hij bij via het id.
    const handleChange = (e) => {
        if(e.target.checked === true && admin.includes(e.target.id) === false){
                admin.push(e.target.id)
        } else if(e.target.checked === false && admin.includes(e.target.id) === true ){
            const index = admin.indexOf(e.target.id)
            if(index > -1 ){
                admin.splice(index, 1);
            }
        }
    }

    
    const handleSubmit = (e) => {
        const existingTokens = JSON.parse(localStorage.getItem("tokens"));
        const token = existingTokens.access_token;
        e.preventDefault();
        //for loop wat ervoor zorgd alle leden die admin mogen worden naar de database pusht.
        for(let i = 0; i < admin.length; i++){
            const data = admin[i]
            axios.put(`http://127.0.0.1:8000/api/auth/user/update/`+ data , data , {
                headers: { 
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json' 
                }
            })
            .then(function (res){
                window.location.reload();
            })
            .catch(function (error){
                window.location.reload();
            });
        }

        
    }
    
    return (
        <Fragment>
        <Navigation />
            <article className="Changerole__article">
                <h1 className="Changerole__article__h1">Role instellingen</h1>
                <p className="Changerole__article__p">Selecteer wie er admin mogen worden</p>
                <button onClick={handleSubmit} type="submit" className="ChangeRole__Button" >Update</button>
            </article>  
            <section className="Changerole">
            <ul className="ChangeRole__ul">
                {leden.map(user =>
                    <li className="ChangeRole__ul__listItem" key={user.id}>
                        <p>{user.Naam} {user.Achternaam}</p>
                        <label
                        htmlFor= {user.id} 
                        className="ChangeRole__ul__listItem__label" 
                        >                      
                        <input
                        id={user.id}
                        name="checkbox"
                        onChange={handleChange}
                        className="ChangeRole__ul__listItem__input" 
                        type="checkbox"
                        />
                        </label>
                    </li>)}
            </ul>
        </section>
        </Fragment>
        )
    }
export default ChangeRole;