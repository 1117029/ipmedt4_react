import React, { useState, useImperativeHandle, forwardRef } from "react";
import ReactDOM from "react-dom";
import "../Groep/groepCard/groepModal.css";
import { useForm } from "react-hook-form";
import axios from "axios";
import useDatafetching from "./../Groep/FetchDataGroep";

const subGroepModal = forwardRef((props, ref) => { 
  const TEST_URL = "http://127.0.0.1:8000/api/";
  
  const gebruiker = JSON.parse(localStorage.getItem("userInfo"));

  const { verenegingId } = useDatafetching(
    TEST_URL + gebruiker.VerenigingNaam + "/verenigingid"
  );
   
  const { groepId } = useDatafetching(
    TEST_URL + verenegingId + "/" + props.g + "/id"
  );

  const { register, handleSubmit } = useForm({

  });

  const onSubmit = data => {

    axios
      .post("http://127.0.0.1:8000/api/subgroep/create ", data, {
        headers: { "Content-Type": "application/json" },
      })
      .then(res => {
        console.log(res.data);
      })
      .catch(error => {
        console.log(error.response);
      });
      close();
  };

  const [display, setDisplay] = useState(false);

  useImperativeHandle(ref, () => {
    return {
      openModal: () => open(),
      close: () => close(),
    };
  });

  const open = () => {
    setDisplay(true);
  };

  const close = () => {
    setDisplay(false);
  };

  if (display) {
    return ReactDOM.createPortal(
      <article className="groepModal">
        <section className="groepModal__scherm">
          <button onClick={close} className="groepModal__scherm__close">
            X
          </button>
          <form className="groepForm" onSubmit={handleSubmit(onSubmit)}>
            <label>Subgroep Naam</label>
            <input
              type="text"
              placeholder="Naam"
              name="Subgroepsnaam"
              ref={register}
            />
            <input
              className="ID"
              type="number"
              value={groepId}
              name="groep_id"
              ref={register}
            />
            <button className="groepForm__button">Voeg Toe</button>
          </form>
        </section>
      </article>,
      document.getElementById("modal-root")
    );
  }
  return null;
});

export default subGroepModal;
