import React, { Fragment } from "react";
import Navigation from "./../../Navigatie/topAppBar/Navigation";
import useDatafetching from "./../Groep/FetchDataGroep";
import DisplayLeden from "../Groep/GroepIndelen/DisplayLeden";
import { Link, useHistory } from "react-router-dom";
import { ReactComponent as TerugIcon } from "../../Icons/arrow_back-24px.svg";

const SubgroepLeden = ({ match }) => {
  const {
    params: { groepsNaam, subgroepNaam },
  } = match;

  const TEST_URL = "http://127.0.0.1:8000/api/";

  const gebruiker = JSON.parse(localStorage.getItem("userInfo"));

  const { user } = useDatafetching(
    TEST_URL +
      gebruiker.VerenigingNaam +
      "/" +
      groepsNaam +
      "/" +
      subgroepNaam +
      "/delete"
  );
  
  const terug = "/groep/" + groepsNaam + "/subgroep";
  return (
    <Fragment>
      <Navigation />
      <div className="Terug">
        <Link to={terug}>
          <TerugIcon />
          <span>Terug</span>
        </Link>
      </div>

      <article className="Leden__Article">
        <h1>Leden {subgroepNaam}</h1>
        <p>
          Dit is de <b>subgroep pagina</b> hier zie je wie er in de subgroep zitten en kan leden verwijderen en toevoegen      
        </p>
        <Link to="add" replace>
          <button className="LedenGrid__addUser">Voeg Een Lid Toe</button>
        </Link>
        <Link to="delete" replace>
          <button className="LedenGrid__deleteUser">Verwijder Een Lid</button>
        </Link>

        <p>
          Deze groep heeft <b>{user.length}</b> lid/leden
        </p>
      </article>
      <div className="Leden__Grid">
        {user.map(user => (
          <DisplayLeden
            key={user.id}
            naam={user.Naam}
            achternaam={user.Achternaam}
            geboortedatum={user.Datum}
          />
        ))}
      </div>
    </Fragment>
  );
};
export default SubgroepLeden;
