import React, { Fragment } from "react";
import { Link } from "react-router-dom";

const SubrGoepcard = ({
  title,
  subTitle,
  leden,
  linkAdd,
  linkDelete,
  linkLeden,
}) => {
  return (
    <Fragment>
      <section className="cardContainer">
        <article className="cardContainer__groepCard">
          <header className="cardContainer__groepCard__header">
            <h2 className="cardContainer__groepCard__header__h3">
              {title || "Groep"}
            </h2>
            <h3 className="cardContainer__groepCard__header__h4">
              {subTitle || "Vereniging"}
            </h3>
          </header>
          <Link to={linkLeden}>
            <button className="cardContainer__groepCard__leden__ledenButton">
              Bekijk Leden
            </button>
          </Link>
        </article>
      </section>
    </Fragment>
  );
};

export default SubrGoepcard;
