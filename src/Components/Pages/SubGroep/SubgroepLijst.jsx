import React, { Fragment, useRef } from "react";
import Navigation from "../../Navigatie/topAppBar/Navigation";
import SubGroepcard from "./subgroepCard";
import useDatafetching from "./../Groep/FetchDataGroep";
import SubGroepModal from "./subGroepModal";
import { Link } from "react-router-dom";
import { ReactComponent as TerugIcon } from "../../Icons/arrow_back-24px.svg";

const Subgroeplijst = ({ match }) => {
  const {
    params: { groepsNaam },
  } = match;

  const TEST_URL = "http://127.0.0.1:8000/api/";

  const userInfo = JSON.parse(localStorage.getItem("userInfo"));

  const { subGroepMap } = useDatafetching(
    TEST_URL + userInfo.VerenigingNaam + "/" + groepsNaam + "/subgroep"
  );

  const groepModalRef = useRef();

  const groepAdd = () => {
    groepModalRef.current.openModal();
  };

  return (
    <Fragment>
      <Navigation />
      <div className="Terug">
        <Link to="/groep">
          <TerugIcon />
          <span>Terug</span>
        </Link>
      </div>
      <article className="groepen">
        <h1>Subgroepen van {groepsNaam}</h1>
        <p>
            Dit is de <b>subgroepen pagina</b> hier zie je wie er in de subgroep zitten en hier kan je leden toevoegen of verwijderen uit de subgroep
        </p>
        <button onClick={groepAdd} className="groep__addButton">
          Voeg een Subgroep Toe
        </button>

      </article>

      <section className="groepGrid">
        {subGroepMap.map(subgroep => (
          <SubGroepcard
            key={subgroep.id}
            title={subgroep.Subgroepsnaam}
            subTitle={userInfo.VerenigingNaam}
            leden={""}
            linkLeden={subgroep.Subgroepsnaam + "/leden"}
            linkAdd={subgroep.Subgroepsnaam + "/add"}
            linkDelete={subgroep.Subgroepsnaam + "/delete"}
          />
        ))}
      </section>
      <SubGroepModal g = {groepsNaam} ref={groepModalRef} />
    </Fragment>
  );
};

export default Subgroeplijst;
