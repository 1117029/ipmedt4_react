import React, {useState,useEffect} from 'react';
import './ingeleverdeActiviteiten.css';
import axios from 'axios';
import Collapsible from 'react-collapsible';
import Navigation from '../../Navigatie/topAppBar/Navigation';

// iconofy icons
import baselineHowToVote from '@iconify/icons-ic/baseline-how-to-vote';
import handThumbsDown from '@iconify/icons-bi/hand-thumbs-down';
import handThumbsUp from '@iconify/icons-bi/hand-thumbs-up';
import { Icon } from '@iconify/react';


const IngeleverdeActiviteiten = (props) => {
    const [data, setData] = useState([]);
    
    const useFetch = (url) => {
        useEffect(()=>{
            setData([]);
            axios.get(url).then(
                res => {
                    console.log(res.data)
                    setData(res.data)
            })
            .catch(error => {
                console.log(error.response)
            });
        },[url])
    }
    useFetch('http://127.0.0.1:8000/api/ingeleverd');

    const post = (url, data, keuren, status) => {
        
        axios.post(url,{
            ActiviteitNaam: data.ActiviteitNaam,
            ActiviteitId: data.ActiviteitId,
            tekst: data.tekst,
            rating: data.rating,
            beoordeeld: keuren,
            NakijkStatus: status,
        }).then(
            res => {
                console.log(res)
            })
            .catch(error => {
                console.log(data)
                console.log(error.response)
        });
    }
    
    // moet de goepsnaam binnen krijgen en de activiteit naam 
    const postKeuren = (activiteit, keuren, status ) => {
        post('http://127.0.0.1:8000/api/inleveren/update/goedkeuren', activiteit, keuren, status)
    }

    const download = (bestand) =>{
        if(bestand === "empty"){
            window.alert("er zit geen bestand in");
        }else{
            axios({
                url: 'http://127.0.0.1:8000/api/inleveren/download/'+ bestand,
                method: 'GET',
                responseType: 'blob',
            }).then((response) => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', bestand.slice(0,bestand.indexOf('.')) + bestand.slice(bestand.indexOf('.')));
                document.body.appendChild(link);
                link.click();
            });
        }
    }

    return (
        <section className='ingeleverd'>
            <Navigation/>
            <h1 className='ingeleverd__titel'>Ingeleverde activiteiten</h1>
            <section className='ingeleverd__container'>
            {data.map((activiteit) => {
                return <section className='ingeleverdKaart'>
                    <section className='ingeleverdKaart__boven'>
                        <aside className="ingeleverdKaart__boven__aside">
                            <h2 className='ingeleverdKaart__boven__aside-titel'>{activiteit.ActiviteitNaam} | </h2>
                            <p className='ingeleverdKaart__onder__aside-subgroep'>Welpen </p>
                        </aside> 
                        <p className='ingeleverdKaart__boven__knop__rating'> <Icon icon={baselineHowToVote} /> {activiteit.rating}/5 </p>
                    </section>
                    <section className='ingeleverdKaart__onder'>
                        <Collapsible className='ingeleverdKaart__boven__knop' trigger = 'Lees meer'>
                            <p className='ingeleverdKaart__boven__knop__beschrijving'> {activiteit.tekst} </p>
                            <section className="ingeleverdKaart__boven__knop__button">
                                <button onClick={()=>download(activiteit.bestand)} className='ingeleverdKaart__boven__knop__bestand'> download het bestand </button>
                                <section className="ingeleverdKaart__boven__knop__buttons">
                                    <button onClick={()=>postKeuren(activiteit, 'afkeuren', 'nietAf')} className='ingeleverdKaart__onder__afkeuren'> <Icon className="ingeleverdKaart__onder__afkeuren-icon" icon={handThumbsDown} /> </button>
                                    <button onClick={()=>postKeuren(activiteit, 'goedkeuren', 'af')} className='ingeleverdKaart__onder__goedkeuren'> <Icon className="ingeleverdKaart__onder__goedkeuren-icon" icon={handThumbsUp} /> </button>
                                </section>
                            </section>
                        </Collapsible>
                    </section>
                </section>
                })  
            }
            </section>
        </section>
    )
}
export default IngeleverdeActiviteiten;
