import React, { useState, useEffect, forwardRef, useImperativeHandle } from "react";
import "./activity.css";
import { useForm} from "react-hook-form";
import axios from "axios";
import ReactDOM from "react-dom";
import Select from 'react-select';

const CreateActivity = forwardRef((props, ref) => {

  const [options, setOptions] = useState("er al iets in");

  const optionsList = [
  ];

  const customStyles = {
    control: styles => ({ ...styles, backgroundColor: 'white', max_width: '100' }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => { 
      return {
        color: isFocused ? 'white' : 'green',
        backgroundColor: isFocused ? 'blue' : 'white',
      }
    }
  }


  const { register, handleSubmit, setValue } = useForm();
  useEffect(() => {
    axios
    .get("http://127.0.0.1:8000/api/zeeverkenners/subgroep",{})
    .then(res => {      
      let subGroepList = res.data[0].subgroep;
      for (let index = 0; index < subGroepList.length; index++) {
        const element = subGroepList[index];        
        optionsList.push({value: element.Subgroepsnaam, label: element.Subgroepsnaam, id: element.id});
      }
      simplFunc();
    })
    .catch(error => {
      console.log(error.response);
    });
  }, []);


  const simplFunc = () => {
    setOptions(optionsList)
  }


  const [values, setReactSelectValue] = useState({ selectedOption: [] });

 


  const handleMultiChange = selectedOption => {
    setValue("reactSelect", selectedOption);
    setReactSelectValue({ selectedOption });
  }

  React.useEffect(() => {

    register({ name: "reactSelect" }); 
    register({ name: "AntdInput" }); 
  }, [register])



  function postMethod (activiteit) {
      axios
      .post("http://127.0.0.1:8000/api/maakpivotactiviteit", activiteit, {
        headers: { "Content-Type": "application/json" },
      })
      .then(res => {
        console.log(res);
        return res;
      })
      .catch(error => {
        console.log(error.response);
      });
  }

  const postActiviteit = (URL, data) =>{
    let postedList = [];
    
    axios.post(URL, data)
    .then(
        res => {
        console.log(res.data);
        let activiteitId = res.data.id;
        for (let index = 0; index < data.reactSelect.length; index++) {
          const element = data.reactSelect[index];
          
          let obj = {'subgroep_id': element.id, 'activiteit_id': activiteitId};
          obj =  JSON.stringify(obj);
          data.Subgroepsnaam = element.value;
          let item = postMethod(obj);
          postedList.push(item);
        }
        if(postedList.length === data.reactSelect.length){
        }
        }
    ).catch(error => {
        console.log(error.response);
    });

    
}



const onSubmit = (data) => {
    let reader = new FileReader();

    if(data.bestand[0] === undefined){     
      postActiviteit('http://127.0.0.1:8000/api/uploadimg', data);

    }else{
      reader.readAsDataURL(data.bestand[0]);
      reader.onload=(e)=>{
        const formData={file:e.target.result};
        data.bestand = formData.file;
        postActiviteit('http://127.0.0.1:8000/api/uploadimg', data);
    }
    }
    close();
    

}

  const [display, setDisplay] = React.useState(false);

    useImperativeHandle(ref, () => {
        return {
            openModal: () => open(),
            close: () => close()
        }
    });

    const open = () => {
        setDisplay(true)
    };
    
    const close = () => {
        setDisplay(false);
    }
    
  if (display) {
    return ReactDOM.createPortal(
      <section className="container">
        <section className="container__formDiv">
          <form className="container__formDiv__form" method="post" onSubmit={handleSubmit(onSubmit)}>
          <h4 className="container__formDiv__form__titel"> Activiteit aanmaken </h4>
            <button onClick={close} className ="container__formDiv__form__sluit"> X </button>
            
            <label htmlFor="container__input" className="container__formDiv__form__label"> Activiteitnaam 
              <input
                className="container__input container__input1"
                type="text"
                name="ActiviteitNaam"          
                ref={register({required: true})}
              ></input>
            </label>

            <label htmlFor="container__textarea" className="container__formDiv__form__label"> Beschrijving
              <textarea
                className="container__input container__textarea"
                type="text"
                name="beschrijving"
                ref={register({required: true})}
              ></textarea>
            </label>

            <label htmlFor="container__input" className="container__formDiv__form__label"> Aantal punten
              <input
                className="container__input"
                type="number"
                name="TeBehalenPunten"
                ref={register({required: true})}
              ></input>
            </label>

            <label htmlFor="container__input" className="container__formDiv__form__label"> groepen
              <Select className="container__select"
                    autosize={false}
                    styles={customStyles}
                    value={values.selectedOption}
                    options={options}
                    onChange={handleMultiChange}
                    isMulti
              />
            </label>

            <input
              className="container__bijlage"
              type="file"
              name="bestand"
              placeholder="Bijlage"
              ref={register}
            ></input>
            
            <input
              className="container__button"
              type="submit"
              value="Aanmaken"
            ></input>
            
          </form>
        </section>
      </section>, document.getElementById("modal-root"))
    }

    return null;
    
}  );


export default CreateActivity;
