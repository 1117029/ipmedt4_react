import React, { useState } from "react";
import ActiviteitenKaart from "./ActiviteitenKaart";
import InleverActiviteitenKaart from "./InleverKaart";

import Navigation from "../../Navigatie/topAppBar/Navigation";
import CreateActivity from "../createActivity/CreateActivity";
import axios from "axios";

import { useEffect } from "react";
import Collapsible from "react-collapsible";

const ActiviteitenLijst = () => {
  const [show, toggleShow] = React.useState(false);
  const user2 = JSON.parse(localStorage.getItem("userInfo"));
  const [userRole, SetUserRole] = useState(true);

  console.log(user2);
  
  const [actCards, setCards] = useState([]);
  const [naKijkenKaart, setNaKijkenKaart] = useState([]);
  const [afKaart, setAfKaart] = useState([]);

  const [activiteitenTitel, setActiviteitenTitel] = useState("Er zijn geen activiteiten");
  const [naKijkenKaartTitel, setNaKijkenKaartTitel] = useState([]);
  const [afKaartTitel, setAfKaartTitel] = useState([]);


  
  useEffect(() => {
    let baseUrl = "http://127.0.0.1:8000/api/"+user2.id+"/subgroep/activiteiten";
    axios
    .get(baseUrl,  {
      headers: { "Content-Type": "application/json" },
    })
    .then(res => {
      console.log(res);
      console.log(res.data[0].subgroep[0].activiteit);
      createCards(res.data[0].subgroep[0].activiteit)

    })
    .catch(error => {
      console.log(error);
      setActiviteitenTitel("Je zit nog niet in een subgroep")
      return error.response
    });
  }, []);



  const createCards = (activiteitenList) => {
    let activiteiten = [];
    let naTeKijkenActiviteiten = [];
    let gemaakteActiviteiten = [];

    activiteitenList.forEach(element => {
      if(element.Status === 'af'){
        gemaakteActiviteiten.push(element);
      }else if(element.Status === 'nakijken'){
        naTeKijkenActiviteiten.push(element);
      }else{
        activiteiten.push(element);
      }
    });

    

    if(activiteiten.length > 0){
      setActiviteitenTitel("Nog " + activiteiten.length + " activiteit(en) maken");
    }
    if(naTeKijkenActiviteiten.length > 0){
      setNaKijkenKaartTitel("Er worden nu " + naTeKijkenActiviteiten.length + " activiteit(en) nagekeken");
    }
    if(gemaakteActiviteiten.length > 0){
      setAfKaartTitel("Er zijn " + gemaakteActiviteiten.length + " activiteit(en) nagekeken");
    }

    setCards(activiteiten);
    setNaKijkenKaart(naTeKijkenActiviteiten);
    setAfKaart(gemaakteActiviteiten)
  } 


  useEffect(() => {
    if(user2.Role === "lid"){
      SetUserRole(false);
    }
  })

  const modalRef = React.useRef();

  const activiteitAdd = () => {
    modalRef.current.openModal();
  };

  return (
    <section className="activiteitenpagina">
      <section className="activiteitenpagina__activiteiten">
      <Navigation />
        <section className="activiteitenpagina__activiteiten__grid">
        <section className="activiteitenpagina__activiteiten__grid__onderdeel">
          <h5 className="activiteitenpagina__activiteiten__grid__onderdeel__titel">{activiteitenTitel}</h5>
          {actCards.map((activiteitKaart) => { 
                    return <ActiviteitenKaart
                    titel={activiteitKaart.ActiviteitNaam}
                    punten={activiteitKaart.TeBehalenPunten}
                    beschrijving={activiteitKaart.Beschrijving}
                    bijlage={activiteitKaart.Bijlage}
                    activiteitId={activiteitKaart.id}
                    />
          })}
        </section>

        <section className="activiteitenpagina__activiteiten__grid__onderdeel">
          <h5 className="activiteitenpagina__activiteiten__grid__onderdeel__titel">{naKijkenKaartTitel}</h5>
          {naKijkenKaart.map((activiteitKaart) => {
                      return <InleverActiviteitenKaart
                      titel={activiteitKaart.ActiviteitNaam}
                      punten={activiteitKaart.TeBehalenPunten}
                      beschrijving={activiteitKaart.Beschrijving}
                      bijlage={activiteitKaart.Bijlage}
                      activiteitId={activiteitKaart.id}
                    />
            })}
        </section>

        <section className="activiteitenpagina__activiteiten__grid__onderdeel">
          <h5 className="activiteitenpagina__activiteiten__grid__onderdeel__titel">{afKaartTitel}</h5>
          {afKaart.map((activiteitKaart) => {
                      return <InleverActiviteitenKaart
                      titel={activiteitKaart.ActiviteitNaam}
                      punten={activiteitKaart.TeBehalenPunten}
                      beschrijving={activiteitKaart.Beschrijving}
                      bijlage={activiteitKaart.Bijlage}
                      activiteitId={activiteitKaart.id}
                    />
            })}
        </section>

        {}
      </section></section>
      {userRole ? (
              <section className="activiteitenpagina__activiteiten__createActivity">
                <button
                  className="activiteitenpagina__activiteiten__createActivity__knop"
                  onClick={activiteitAdd}>
                  {" "}
                  +{" "}
              </button>
            </section>

           ) : (
            <h5 className="activiteitenpagina__activiteiten--hiddenh5">dit is een nee</h5>
           )}


      <CreateActivity ref={modalRef}></CreateActivity>
    </section>
  );
};

export default ActiviteitenLijst;
