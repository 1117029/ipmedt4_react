import React from 'react';
import './ActiviteitenKaart.css'
import Collapsible from 'react-collapsible';
import axios from 'axios';

import Inleveren from "./Inleveren";
import quality from "../../Icons/quality.svg";

const ActiviteitenKaart = (props) => {

    const download = (bestand) =>{
        if(bestand === "empty"){
        }else{
            axios({
                url: 'http://127.0.0.1:8000/api/activiteit/download/'+ bestand,
                method: 'GET',
                responseType: 'blob', 
              }).then((response) => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', bestand.slice(0,bestand.indexOf('.')) + bestand.slice(bestand.indexOf('.')));
                document.body.appendChild(link);
                link.click();
              });
            
        }
    }

    const modalRef = React.useRef();

    const inleveren = () => {
         modalRef.current.openModal()
     }
    
    return(
        <section className='activiteitenKaart'>
            <div className='activiteitenKaart__links'>
                <h5 className='activiteitenKaart__links__titel'> {" "} {props.titel}</h5>
                <Collapsible className='activiteitenKaart__links__knop' trigger = 'Lees meer'>
                    <p className='activiteitenKaart__links__knop__beschrijving'> {" "} {props.beschrijving}</p>
                    <button onClick={()=>download(props.bijlage)} className="activiteitenKaart__links__knop__pdf"> {" "} {props.bijlage}</button>
                </Collapsible>
                <quality />
            </div>
            <div className='activiteitenKaart__rechts'> 
                <h5 className='activiteitenKaart__rechts__punten'> {""}  {props.punten}
                    <img className='activiteitenKaart__rechts__punten__img' src={quality} alt='punten'></img> 
                </h5>
                <button className='activiteitenKaart__rechts__knop' onClick={inleveren}> Inleveren </button>
            </div>
            <Inleveren ref={modalRef} className="inleveren"> 
                <h4 className="inleveren__titel">{props.titel}</h4>
                <p className="inleveren__tekst">{props.activiteitId}</p>
            </Inleveren>
        </section>
    );
    
}

export default ActiviteitenKaart;
