import React, { useState, useContext } from "react";
import { Link, Redirect } from "react-router-dom";
import { useAuth } from "../../Context/auth";
import { GrLicense } from "react-icons/gr";
import { GrUser } from "react-icons/gr";
import { IconContext } from "react-icons";
import axios from "axios";
import { useForm } from "react-hook-form";
import "./loginScreen.css";

import { UserContext } from "../../Context/user";

const LoginScreen = () => {
  const { register, handleSubmit, errors } = useForm();
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [isError, setIsError] = useState(false);
  const { setAuthTokens } = useAuth();
  const { setUser } = useContext(UserContext);

  const TEST_URL = "http://127.0.0.1:8000/";

  const onSubmit = data => {
      axios
      .post(TEST_URL + "api/auth/login", data, {
        headers: { "Content-Type": "application/json" },
      })
      .then(res => {
        if (res.status === 200) {
          //dit is de functie om de user op te vragen aan de database
          const token = res.data.access_token;
          axios
          .get(TEST_URL + "api/auth/user/ ", {
            headers: { Authorization: `Bearer ${token}` },
          })
          .then(data => {
            //set user is een context hook die ervoor zorgt dat de user in alle componenten aangesproken kan worden
            setUser(data.data);
          })
          .catch(error => {
          });
          setAuthTokens(res.data);
          setLoggedIn(true);
        } else {
          setIsError(true);
        }
      })
      .catch(error => {
        setIsError(true);
      });
    };

  if (isLoggedIn) {
    return <Redirect to="/home" />;
  }

  return (
    <section className="login">
      <article className="login__container">
      <form className="login__container__form" onSubmit={handleSubmit(onSubmit)}>
        <h1>Login</h1>
        <section className="login__container__form__group">
          <label className="login__container__form__group__label" htmlFor="">
            Email
          </label>
          <IconContext.Provider value={{ className: "icon" }}>
            <figure className="login__container__form__icon">
              <GrUser />
            </figure>
          </IconContext.Provider>
          <input
            id="inlog-email"
            className="login__container__form__group__input"
            type="text"
            name="Email"
            ref={register({required: "Email vereist", minLength: {value: 1, message: "Email vereist"}})}
            />
          {errors.Email && <p className='error'>{errors.Email.message}</p>} 
        </section>
        <section className="login__container__form__group">
          <label className="login__container__form__group__label" htmlFor="Wachtwoord">
            Wachtwoord
          </label>
          <IconContext.Provider value={{ className: "icon" }}>
            <figure className="login__container__form__icon">
              <GrLicense />
            </figure>
          </IconContext.Provider>
          <input
            id="inlog-wachtwoord"
            className="login__container__form__group__input"
            type="password"
            name="password"
            ref={register ({required: "Wachtwoord vereist"})}
            />
          {errors.password && <p className='error'>{errors.password.message}</p>}
        </section>
        <section className="login__container__form__group">

            <input
            id="inlog-checkbox"
            className="login__container__form__group__checkbox"
            type="checkbox"/>

            <label 
            className="login__container__form__group__CB-label" 
            htmlFor="inlog-checkbox"> 
            Onthoud mijn gegevens
            </label>
        </section>
        <section className="login__container__form__error"> 
            {isError && <p className="login__container__form__error__p"> <i> Het Email address of wachtwoord is niet correct</i></p>}
          </section>
        <section className="login__container__form__group">
          <button type="submit" id="login-submit" className="login__container__form__group__login">
            Login
          </button>
        </section>
        <Link className="login__sec__account__maken" to="/register">Account aanmaken</Link>
      </form>
      </article>
    </section>
  );
};

export default LoginScreen;
