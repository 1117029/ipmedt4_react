import React, { Component, useState, useRef } from "react";
import "./RegisterForm.css";
import axios from "axios";
import { useForm } from "react-hook-form";
import { Link, Redirect, useHistory  } from "react-router-dom";
import { GrUser } from "react-icons/gr";
import { GrSchedule } from "react-icons/gr";
import { GrMailOption } from "react-icons/gr";
import { GrLicense } from "react-icons/gr";
import { GrAttraction } from "react-icons/gr";
import { IconContext } from "react-icons";

const RegisterForm2 = () => {
  const { register, handleSubmit, errors, watch } = useForm();
  const password = useRef({});
  const TEST_URL = "http://127.0.0.1:8000/";
  password.current = watch("password", "");
  const history = useHistory();

  const onSubmit = data => {
    console.log(data);
    axios.defaults.xsrfCookieName = "csrftoken";
    axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
    axios
      .post(TEST_URL + "api/auth/signup", data, {
        headers: { "Content-Type": "application/json" },
      })
      .then(res => {
        console.log(res.data);
        history.push("/");

      })
      .catch(error => {
        console.log(error.response);
      });
  };

  return (
    <section className="register">
      <div id="register__container">
      <div className="register_terug">
          <Link to="/">&#8592;Terug</Link>
        </div>
        <form id="register__form" onSubmit={handleSubmit(onSubmit)}>

          <h1 id="register__titel">Register</h1>

          <label className="register__label">
            <h3 className="register__label__tekst">Voornaam</h3>
            <IconContext.Provider value={{ className: "icon" }}>
              <div>
                <GrUser />
              </div>
            </IconContext.Provider>
            <input
              className="register__input"
              type="text"
              name="Naam"
              ref={register({
                required: "Naam vereist",
                minLength: { value: 1, message: "Naam vereist" },
              })}
            />
            {errors.Naam && <p className="error">{errors.Naam.message}</p>}
          </label>

          <label className="register__label">
            <h3 className="register__label__tekst">Achternaam</h3>
            <IconContext.Provider value={{ className: "icon" }}>
              <div>
                <GrUser />
              </div>
            </IconContext.Provider>
            <input
              className="register__input"
              type="text"
              name="Achternaam"
              ref={register({
                required: "Achternaam vereist",
                minLength: { value: 1, message: "Achternaam vereist" },
              })}
            />
            {errors.Achternaam && (
              <p className="error">{errors.Achternaam.message}</p>
            )}
          </label>

          <label class="register__label">
            <h3 className="register__label__tekst register__label__tekst2">
              Geboorte Dag
            </h3>
            <IconContext.Provider value={{ className: "icon icon2" }}>
              <div>
                <GrSchedule />
              </div>
            </IconContext.Provider>
            <input
              className="register__datum"
              type="date"
              name="Datum"
              ref={register({
                required: "geboortedatum vereist",
                pattern: {
                  value: /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/,
                  message: "Ongeldig geboortedatum",
                },
              })}
            />
            {errors.Datum && <p className="error">{errors.Datum.message}</p>}
          </label>

          <label className="register__label">
            <h3 className="register__label__tekst">E-mail</h3>
            <IconContext.Provider value={{ className: "icon" }}>
              <div>
                <GrMailOption />
              </div>
            </IconContext.Provider>
            <input
              className="register__input"
              type="text"
              name="Email"
              ref={register({
                required: "Email vereist",
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                  message: "ongeldig email address",
                },
              })}
            />
            {errors.Email && <p className="error">{errors.Email.message}</p>}
          </label>

          <label className="register__label">
            <h3 className="register__label__tekst">Wachtwoord</h3>
            <IconContext.Provider value={{ className: "icon" }}>
              <div>
                <GrLicense />
              </div>
            </IconContext.Provider>
            <input
              className="register__input"
              type="password"
              name="password"
              ref={register({
                required: "Wachtwoord vereist",
                minLength: {
                  value: 8,
                  message: "Wachtwoord moet uit 8 karakters bestaan",
                },
              })}
            />
            {errors.password && (
              <p className="error">{errors.password.message}</p>
            )}
          </label>

          <label className="register__label">
            <h3 className="register__label__tekst">Wachtwoord bevestiging</h3>
            <IconContext.Provider value={{ className: "icon" }}>
              <div>
                <GrLicense />
              </div>
            </IconContext.Provider>
            <input
              className="register__input"
              type="password"
              name="password_confirmation"
              ref={register({
                required: "Confirmatie vereist",
                validate: value =>
                  value === password.current || "Wachtwoord niet hetzlefde",
              })}
            />
            {errors.password_confirmation && (
              <p className="error">{errors.password_confirmation.message}</p>
            )}
          </label>

          <label className="register__label">
            <h3 className="register__label__tekst">Vereniging code</h3>
            <IconContext.Provider value={{ className: "icon" }}>
              <div>
                <GrAttraction />
              </div>
            </IconContext.Provider>
            <input
              className="register__input"
              type="text"
              name="Vereniging_code"
              ref={register({
                required: "Code vereist",
                length: { value: 5, message: "Ongeldige code" },
              })}
            />
            {errors.Vereniging_code && (
              <p className="error">{errors.Vereniging_code.message}</p>
            )}
          </label>

          <button type="submit" className="register__button">
            Maak account
          </button>
        </form>
      </div>
    </section>
  );
};

export default RegisterForm2;
