import React, { Fragment, useHistory } from "react";
import "./groepCard.css";
import { Link } from "react-router-dom";

const groepcard = ({ title, subTitle, linkSubgroepen, Linkleden }) => {
  return (
    <Fragment>
      <section className="cardContainer">
        <article className="cardContainer__groepCard">
          <header className="cardContainer__groepCard__header">
            <h3 className="cardContainer__groepCard__header__h3">
              {title || "Groep"}
            </h3>
            <h4 className="cardContainer__groepCard__header__h4">
              {subTitle || "Vereniging"}
            </h4>
          </header>
          <Link to={linkSubgroepen}>
            <button className="cardContainer__groepCard__leden__subGroepButton">
              Bekijk Subgroep
            </button>
          </Link>
          <Link to={Linkleden}>
            <button className="cardContainer__groepCard__leden__ledenButton">
              Bekijk Leden
            </button>
          </Link>
        </article>
      </section>
    </Fragment>
  );
};

export default groepcard;
