import React, { useState, useImperativeHandle, forwardRef } from "react";
import ReactDOM from "react-dom";
import "./groepModal.css";
import { useForm } from "react-hook-form";
import axios from "axios";
import useDatafetching from "../FetchDataGroep";

const groepModal = forwardRef((props, ref) => {

  const TEST_URL = "http://127.0.0.1:8000/api/";

  const gebruiker = JSON.parse(localStorage.getItem("userInfo"));
  const { verenegingId } = useDatafetching(
    TEST_URL + gebruiker.VerenigingNaam + "/verenigingid"
  );
  const { register, handleSubmit } = useForm({
  });

  const onSubmit = data => {

    axios
      .post("http://127.0.0.1:8000/api/groep/create ", data, {
        headers: { "Content-Type": "application/json" },
      })
      .then(res => {
        console.log(res.data);
      })
      .catch(error => {
        console.log(error.response);
      });

      close();
  };

  const [display, setDisplay] = useState(false);

  useImperativeHandle(ref, () => {
    return {
      openModal: () => open(),
      close: () => close(),
    };
  });

  const open = () => {
    setDisplay(true);
  };

  const close = () =>{
    setDisplay(false);
  };

  if (display) {
    return ReactDOM.createPortal(
      <article className="groepModal">
        <section className="groepModal__scherm">
          <button onClick={close} className="groepModal__scherm__close">
            X
          </button>
          <form className="groepForm" onSubmit={handleSubmit(onSubmit)}>
      <label>Groep Naam</label>
      <input type="text" placeholder="Naam" name="GroepsNaam" ref={register} />
      <input
        className="ID"
        type="number"
        defaultValue={verenegingId}
        name="vereniging_id"
        ref={register}
      />
      <button className="groepForm__button">Voeg Toe</button>
    </form>
        </section>
      </article>,
      document.getElementById("modal-root")
    );
  }
  return null;
});

export default groepModal;