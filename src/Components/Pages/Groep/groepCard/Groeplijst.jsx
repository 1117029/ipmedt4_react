import React, { Fragment, useRef } from "react";
import Navigation from "../../../Navigatie/topAppBar/Navigation";
import GroepCard from "./groepCard";
import useDatafetching from "./../FetchDataGroep";
import GroepModal from "./groepModal";
import { Link } from "react-router-dom";

const Groeplijst = () => {
  const TEST_URL = "http://127.0.0.1:8000/api/";

  const gebruiker = JSON.parse(localStorage.getItem("userInfo"));
  const { groepMap } = useDatafetching(
    TEST_URL + gebruiker.VerenigingNaam + "/groep"
  );

  const groepModalRef = useRef();

  const groepAdd = () => {
    groepModalRef.current.openModal();
  };

  return (
    <Fragment>
      <Navigation />
      <article className="groepen">
        <h1>Groepen</h1>
        <p>
            Dit is de <b>groepen pagina</b> klik op een knop om hier de groep/subgroep informatie te bekijken of om de leden aan te passen<br></br>
        </p>
        <button onClick={groepAdd} className="groep__addButton">
          Voeg een Groep Toe
        </button>
      </article>

      <section className="groepGrid">
        {groepMap.map(groep => (
          <GroepCard
            key={groep.id}
            title={groep.GroepsNaam}
            leden={""}
            subTitle={gebruiker.VerenigingNaam}
            Linkleden={"/groep/" + groep.GroepsNaam}
            linkSubgroepen={"/groep/" + groep.GroepsNaam + "/subgroep"}
          />
        ))}
      </section>
      <GroepModal ref={groepModalRef} />
    </Fragment>
  );
};

export default Groeplijst;
