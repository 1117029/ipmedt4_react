import React, { Fragment } from "react";
import DisplayLeden from "../GroepIndelen/DisplayLeden";
import { Link } from "react-router-dom";
import Navigation from "./../../../Navigatie/topAppBar/Navigation";
import useDatafetching from "./../FetchDataGroep";
import { ReactComponent as TerugIcon } from "../../../Icons/arrow_back-24px.svg";

const SubgroepLeden = ({ match }) => {
  const {
    params: { groepsNaam },
  } = match;

  const TEST_URL = "http://127.0.0.1:8000/api/";

  const gebruiker = JSON.parse(localStorage.getItem("userInfo"));

  const { user } = useDatafetching(
    TEST_URL + gebruiker.VerenigingNaam + "/" + groepsNaam + "/users"
  );
  const linkAdd = "/groep/" + groepsNaam + "/add";
  const linkDelete = "/groep/" + groepsNaam + "/delete";
  return (
    <Fragment>
      <Navigation />
      <div className="Terug">
        <Link to="/groep">
          <TerugIcon />
          <span>Terug</span>
        </Link>
      </div>
      <article className="Leden__Article">
        <h1>Leden {groepsNaam}</h1>
        <p>
            Dit is de <b>leden pagina</b> hier zie je wie er in de groep zitten en kan je leden verwijderen of toevoegen
        </p>
        <Link to={linkAdd}>
          <button className="LedenGrid__addUser">Voeg Een Lid Toe</button>
        </Link>
        <Link to={linkDelete}>
          <button className="LedenGrid__deleteUser">Verwijder Een Lid</button>
        </Link>
        <p>
            Deze groep heeft <b>{user.length}</b> lid/leden
        </p>
      </article>
      <div className="Leden__Grid">
        {user.map(user => (
          <DisplayLeden
            key={user.id}
            naam={user.Naam}
            achternaam={user.Achternaam}
            geboortedatum={user.Datum}
          />
        ))}
      </div>
    </Fragment>
  );
};
export default SubgroepLeden;
