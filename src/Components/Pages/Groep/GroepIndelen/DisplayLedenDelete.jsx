import React, { Fragment, useState } from "react";
import "./displayleden.css";

const DisplayLedenEdit = ({ naam, achternaam, geboortedatum }) => {
  const [toggle, setToggle] = useState(false);
  return (
    <Fragment>
      <section className="lidCardContainer" onClick={() => setToggle(!toggle)}>
        <article
          className={
            toggle === false
              ? "lidCardContainer__Card"
              : "lidCardContainer__Card__delete"
          }>
          <header className="lidCardContainer__Card__header">
            <h3
              className={
                toggle === false
                  ? "lidCardContainer__Card__header__h4"
                  : "lidCardContainer__Card__header__h4--active"
              }>
              {naam || "naam"} {achternaam || "achternaam"}
            </h3>
            <h4
              className={
                toggle === false
                  ? "lidCardContainer__Card__header__h5"
                  : "lidCardContainer__Card__header__h5--active"
              }>
              {geboortedatum || "geboortedatum"}
            </h4>
          </header>
        </article>
      </section>
    </Fragment>
  );
};

export default DisplayLedenEdit;
