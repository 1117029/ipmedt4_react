import React, { Fragment, useState } from "react";
import "./displayleden.css";

const DisplayLedenEdit = ({ naam, achternaam, geboortedatum }) => {
  const [toggle, setToggle] = useState(false);
  return (
    <Fragment>
      <section className="lidCardContainer" onClick={() => setToggle(!toggle)}>
        <article
          className={
            toggle === false
              ? "lidCardContainer__Card"
              : "lidCardContainer__Card--active"
          }>
          <header className="lidCardContainer__Card__header">
            <h4
              className={
                toggle === false
                  ? "lidCardContainer__Card__header__h4"
                  : "lidCardContainer__Card__header__h4--active"
              }>
              {naam || "naam"} {achternaam || "achternaam"}
            </h4>
            <h5
              className={
                toggle === false
                  ? "lidCardContainer__Card__header__h5"
                  : "lidCardContainer__Card__header__h5--active"
              }>
              {geboortedatum || "geboortedatum"}
            </h5>
          </header>
        </article>
      </section>
    </Fragment>
  );
};

export default DisplayLedenEdit;
