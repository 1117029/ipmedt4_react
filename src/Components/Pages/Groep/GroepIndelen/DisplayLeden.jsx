import React, { Fragment } from "react";
import "./displayleden.css";

const DisplayLeden = ({ naam, achternaam, geboortedatum }) => {
  return (
    <Fragment>
      <section className="lidCardContainer">
        <article className="lidCardContainer__Card">
          <header className="lidCardContainer__Card__header">
            <h4 className="lidCardContainer__Card__header__h4">
              {naam || "naam"} {achternaam || "achternaam"}
            </h4>
            <h5 className="lidCardContainer__Card__header__h5">
              {geboortedatum || "geboortedatum"}
            </h5>
          </header>
        </article>
      </section>
    </Fragment>
  );
};

export default DisplayLeden;
