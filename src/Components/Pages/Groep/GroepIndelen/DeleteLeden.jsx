import React, { Component, Fragment } from "react";
import axios from "axios";
import DisplayLedenEdit from "./DisplayLedenDelete";
import useDatafetching from "../FetchDataGroep";
import Navigation from "./../../../Navigatie/topAppBar/Navigation";
import { useHistory, Link } from "react-router-dom";
import { ReactComponent as TerugIcon } from "../../../Icons/arrow_back-24px.svg";

const GroepLeden = ({ match }) => {
  const {
    params: { groepsNaam },
  } = match;

  let clickedList = [];

  const TEST_URL = "http://127.0.0.1:8000/api/";
  const history = useHistory();
  const terug = "/groep/" + groepsNaam;

  const gebruiker = JSON.parse(localStorage.getItem("userInfo"));
  const { verenegingId } = useDatafetching(
    TEST_URL + gebruiker.VerenigingNaam + "/verenigingid"
  );
  const { groepId } = useDatafetching(
    TEST_URL + verenegingId + "/" + groepsNaam + "/id"
  );
  const { user } = useDatafetching(
    TEST_URL + gebruiker.VerenigingNaam + "/" + groepsNaam + "/users"
  );

  const simpleFunc = value => {
    if (clickedList.some(code => code.user_id === value.user_id)) {
      clickedList = clickedList.filter(code => code.user_id !== value.user_id);
    } else {
      clickedList.push(value);
    }
  };

  const postRequest = clickedList => {
    for (let i = 0; i < clickedList.length; i++) {
      let jsonList = JSON.stringify(clickedList[i]);
      axios
        .put(TEST_URL + "groep/users/delete", jsonList, {
          headers: { "Content-Type": "application/json" },
        })
        .then(res => {
          console.log(res);
        })
        .catch(error => {
          console.log(error.response);
        });
    }
  };

  return (
    <Fragment>
      <Navigation />
      <div className="Terug">
        <Link to={terug}>
          <TerugIcon />
          <span>Terug</span>
        </Link>
      </div>

      <section className="veranderGroep">
        <article className="Leden__Article">
          <h1 className="Leden__h1">{groepsNaam} Verwijderen </h1>
          <p>
            Klik op een lid om deze te <b>verwijderen</b> van een groep
          </p>
          <button
            type="submit"
            className="Leden__buttonDelete"
            onClick={() => {
              postRequest(clickedList);
              history.goBack();
            }}>
            Verwijder Leden
          </button>
        </article>
        <div className="Leden__Grid">
          {user.map(user => (
            <div
              onClick={() =>
                simpleFunc({ groep_id: groepId, user_id: user.id })
              }>
              <DisplayLedenEdit
                key={user.id}
                naam={user.Naam}
                achternaam={user.Achternaam}
                geboortedatum={user.Datum}></DisplayLedenEdit>
            </div>
          ))}
        </div>
      </section>
    </Fragment>
  );
};

export default GroepLeden;
