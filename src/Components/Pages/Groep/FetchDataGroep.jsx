import React, { useState, useEffect } from "react";
import Axios from "axios";

const useDatafetching = datasource => {
  const [groep, setGroepen] = useState([]);
  const [error, setError] = useState("");
  const [groepMap, setGroepMap] = useState([]);
  const [subGroepMap, setSubGroepMap] = useState([]);
  const [leden, setLeden] = useState([]);
  const [user, setUser] = useState([]);
  const [verenegingId, setVerenegingId] = useState();
  const [groepId, setGroepId] = useState();
  const [subgroepId, setsubgroepId] = useState();
  const [userGroep, setUserGroep] = useState();
  useEffect(() => {
    async function fetchdata() {
      try {
        const response = await Axios.get(datasource);

        if (response) {
          setVerenegingId(response.data[0].id);
          setGroepId(response.data[0].id);
          setGroepen(response);
          setGroepMap(response.data[0].groep);
          setSubGroepMap(response.data);
          setLeden(response.data);
          setUser(response.data);
          setUserGroep(response.data[0].groep[0]);
        }
      } catch {
        console.log(error.message);
      }
    }
    fetchdata();
  }, [datasource]);

  return {
    error,
    groep,
    groepMap,
    subGroepMap,
    leden,
    user,
    verenegingId,
    groepId,
    userGroep,
  };
};

export default useDatafetching;
