import React, { useState, useEffect, Fragment } from "react";
import { CSSTransition } from "react-transition-group";
import { Link } from "react-router-dom";
import { useAuth } from "../../Context/auth";
import useDatafetching from "../../Pages/Groep/FetchDataGroep";
import axios from "axios";

// ----------------------ICONS------------------
import { ReactComponent as HomeIcon } from "../../Icons/home-24px.svg";
import { ReactComponent as AdminIcon } from "../../Icons/password.svg";
import { ReactComponent as Check } from "../../Icons/task.svg";
import { ReactComponent as Logout } from "../../Icons/logout.svg";
import { ReactComponent as ActiviteitenIcon } from "../../Icons/assignment-24px.svg";
import { ReactComponent as GroepIcon } from "../../Icons/groep.svg";
import { ReactComponent as Team } from "../../Icons/team.svg";
import { ReactComponent as BackIcon } from "../../Icons/arrow_back-24px.svg";

import "./topAppBar.css";
const Dropdown = () => {
  const [activeMenu, setActiveMenu] = useState("main");
  const [menuHeight, setMenuHeight] = useState("null");
  const { setAuthTokens } = useAuth();

  const user = JSON.parse(localStorage.getItem("userInfo"));
  const [userRole, SetUserRole] = useState(true);


  useEffect(() => {
    if(user.Role === "lid"){
      SetUserRole(false);
    }
  }, []);


  const calcHeight = e => {
    const height = e.offsetHeight;
    setMenuHeight(height);
  };

  const TEST_URL = "http://127.0.0.1:8000/api/";
  // ------------------Fetches Groups For Menu------------------
  const verenigingNaam = JSON.parse(localStorage.getItem("userInfo"));
  const { groepMap } = useDatafetching(
    TEST_URL + verenigingNaam.VerenigingNaam + "/groep"
  );
  //--------------------Log Out Function for backend---------------
  const logOut = () => {
    const existingTokens = JSON.parse(localStorage.getItem("tokens"));
    const token = existingTokens.access_token;

    axios
      .get("http://localhost:3000/api/auth/logout", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        console.log(res);
      })
      .catch(error => {
        console.log(error.response);
      });

    setAuthTokens();
    localStorage.removeItem("tokens");
    localStorage.removeItem("userInfo");
  };
  // ---------------DropDown item Component for menu---------------
  const DropdownItems = props => {
    return (
      <div
        className="Dropdown__items"
        onClick={() => props.goToMenu && setActiveMenu(props.goToMenu)}>
        <i className="Dropdown__items__iconLeft">{props.leftIcon}</i>
        {props.children}
        <i className="Dropdown__items__iconRight">{props.rightIcon}</i>
      </div>
    );
  };
  return (
    <Fragment>
      {userRole ? (
        <div className="Dropdown" style={{ height: menuHeight }}>
        <CSSTransition
          in={activeMenu === "main"}
          unmountOnExit
          timeout={500}
          classNames="menu-primary"
          onEnter={calcHeight}>
          <div className="dropdown__menu">
            <DropdownItems leftIcon={<HomeIcon />}>
              <Link to="/home">Home</Link>
            </DropdownItems>
            <DropdownItems leftIcon={<ActiviteitenIcon />}>
              <Link to="/activiteiten">Activiteiten</Link>
            </DropdownItems>
            <DropdownItems leftIcon={<Check />}>
              <Link to="/ingeleverd">ingeleverd</Link>
            </DropdownItems>
            <DropdownItems leftIcon={<AdminIcon />}>
            <Link to="/changerole">Admins</Link>
            </DropdownItems>
            <DropdownItems goToMenu="groep" leftIcon={<GroepIcon />}>
              <p>Groepen</p>
            </DropdownItems>
            <DropdownItems leftIcon={<Logout />}>
              <Link to="/" onClick={logOut}>
                Log Out
              </Link>
            </DropdownItems>
          </div>
        </CSSTransition>

        <CSSTransition
          in={activeMenu === "groep"}
          timeout={500}
          classNames="menu-secondary"
          unmountOnExit
          onEnter={calcHeight}>
          <div className="menu">
            <DropdownItems
              goToMenu="main"
              leftIcon={<BackIcon />}></DropdownItems>
            <DropdownItems leftIcon={<Team />}>
              <Link to="/groep">Alle </Link>
            </DropdownItems>
            {groepMap.map(groep => (
              <DropdownItems key={groep.id} leftIcon={<GroepIcon />}>
                <Link to={"groep/" + groep.GroepsNaam}>{groep.GroepsNaam}</Link>
              </DropdownItems>
            ))}
          </div>
        </CSSTransition>
      </div>
     ) : (
      <div className="Dropdown" style={{ height: menuHeight }}>
        <CSSTransition
          in={activeMenu === "main"}
          unmountOnExit
          timeout={500}
          classNames="menu-primary"
          onEnter={calcHeight}>
          <div className="dropdown__menu">
            <DropdownItems leftIcon={<HomeIcon />}>
              <Link to="/home">Home</Link>
            </DropdownItems>
            <DropdownItems leftIcon={<ActiviteitenIcon />}>
              <Link to="/activiteiten">Activiteiten</Link>
            </DropdownItems>
            <DropdownItems leftIcon={<Logout />}>
              <Link to="/" onClick={logOut}>
                Log Out
              </Link>
            </DropdownItems>
          </div>
        </CSSTransition>

      </div>
     )}

    </Fragment>
  );
};

export default Dropdown;
