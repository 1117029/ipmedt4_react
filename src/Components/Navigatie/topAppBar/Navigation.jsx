import React from "react";
import TopAppBar from "./topAppBar";
import NavItem from "./NavItem";
import Dropdown from "./Dropdown";
import { ReactComponent as MenuIcon } from "../../Icons/menu.svg";

const gebruiker = JSON.parse(localStorage.getItem("userInfo"));

const Navigation = () => {
  return (
    <TopAppBar>
      <p id="menu__vereniging">Scouting</p>
      <p id="menu">MENU</p>
      <NavItem icon={<MenuIcon />}>
        <Dropdown />
      </NavItem>
    </TopAppBar>
  );
};

export default Navigation;
