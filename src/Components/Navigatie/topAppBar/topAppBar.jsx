import React from "react";
import "./topAppBar.css";

const topAppBar = (props) => {
  return (
    <nav className="topNavBar">
      <ul className="navbar__list">{props.children}</ul>
    </nav>

  );
};

export default topAppBar;
