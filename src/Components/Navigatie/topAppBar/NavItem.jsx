import React from "react"
import "./topAppBar.css";
import { useState } from "react";

const NavItem = (props) => {

    const [open, setOpen] = useState(false);
    return (
        <li className="navItem">
            <div href="#" className="navItem__icon" onClick={() => setOpen(!open)}>
            {props.icon}
            </div>
            {open && props.children}

        </li>
    )
}

export default NavItem;