import React from "react";
import "./actionButton.css";
import { IconContext } from "react-icons";
import { MdAdd } from "react-icons/md";

const actionButton = () => {
  return (
    <button className="actionButton">
      <IconContext.Provider value={{ className: "actionButton__icon" }}>
        <i>
          <MdAdd />
        </i>
      </IconContext.Provider>
    </button>
  );
};

export default actionButton;
