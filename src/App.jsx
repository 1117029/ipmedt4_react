import React, { useState, Fragment } from "react";
// ----------------Routing Authentication ------
import {
  BrowserRouter,
  Route,
  Switch,
  Link,
  useParams,
  useHistory,
} from "react-router-dom";
import { AuthContext } from "./Components/Context/auth";
import PrivateRoute from "./Components/PrivateRoutes/PrivateRoute";

// -------------------------Componenten------------------------------
import GroepLijst from "./Components/Pages/Groep/groepCard/Groeplijst";
import SubgroepLijst from "./Components/Pages/SubGroep/SubgroepLijst";
import LoginScreen from "./Components/Pages/Login/loginScreen";
import RegisterForm2 from "./Components/Pages/Register/RegisterForm2";
import Activiteitenlijst from "./Components/Pages/activiteitenkaart/ActiviteitenLijst";
import GroepLeden from "./Components/Pages/Groep/GroepIndelen/GroepLeden";
import CreateActivity from "./Components/Pages/createActivity/CreateActivity";
import Home from "./Components/Pages/Home/Home";
import ChangeRole from "./Components/Pages/ChangeRole/ChangeRole";
import ingeleverdeActiviteiten from "./Components/Pages/ingeleverdeActiviteiten/ingeleverdeActiviteiten";
import "./App.css";
import { UserContext } from "./Components/Context/user";
import { useEffect } from "react";
import DeleteLeden from "./Components/Pages/Groep/GroepIndelen/DeleteLeden";
import AddSubGroepLeden from "./Components/Pages/Subgroep indelen/AddSubGroepLeden";
import DeleteSubGroepLeden from "./Components/Pages/Subgroep indelen/DeleteSubGroepLeden";
import subgroepLeden from "./Components/Pages/SubGroep/subgroepLeden";
import GroepLedenLijst from "./Components/Pages/Groep/groepCard/GroepLedenLijst";

function App(props) {
  const existingTokens = JSON.parse(localStorage.getItem("tokens"));
  const [user, setUser] = useState([]);
  const [authTokens, setAuthTokens] = useState(existingTokens);
  const setTokens = data => {
    localStorage.setItem("tokens", JSON.stringify(data));
    setAuthTokens(data);
  };

  useEffect(() => {
    const userData = localStorage.getItem("userInfo");
    if (userData) {
      setUser(JSON.parse(userData));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("userInfo", JSON.stringify(user));
  });

  return (
    <UserContext.Provider value={{ user, setUser }}>
      <AuthContext.Provider
        value={{
          authTokens,
          setAuthTokens: setTokens,
        }}>
        <BrowserRouter>
          <Switch>
            <Route exact path="/home" component={Home} />
            <Route exact path="/changerole" component={ChangeRole} />
            <Route exact path="/" component={LoginScreen} />
            <Route exact path="/register" component={RegisterForm2} />
            <PrivateRoute exact path="/groep" component={GroepLijst} />
            <PrivateRoute
              exact
              path="/groep/:groepsNaam/"
              component={GroepLedenLijst}
            />
            <PrivateRoute
              exact
              path="/groep/:groepsNaam/add"
              component={GroepLeden}
            />
            <PrivateRoute
              exact
              path="/groep/:groepsNaam/delete"
              component={DeleteLeden}
            />
            <PrivateRoute
              exact
              path="/groep/:groepsNaam/subgroep"
              component={SubgroepLijst}
            />
            <PrivateRoute
              exact
              path="/groep/:groepsNaam/:subgroepNaam/add"
              component={AddSubGroepLeden}
            />

            <PrivateRoute
              exact
              path="/groep/:groepsNaam/:subgroepNaam/leden"
              component={subgroepLeden}
            />
            <PrivateRoute
              exact
              path="/groep/:groepsNaam/:subgroepNaam/delete"
              component={DeleteSubGroepLeden}
            />
            <PrivateRoute
              exact
              path="/activiteiten"
              component={Activiteitenlijst}
            />
            <Route path="/activiteiten+" component={CreateActivity} />
            <Route path="/ingeleverd" component={ingeleverdeActiviteiten} />
          </Switch>
        </BrowserRouter>
      </AuthContext.Provider>
    </UserContext.Provider>
  );
}

export default App;
